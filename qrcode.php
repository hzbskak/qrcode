<?php

    // 查询推荐码
    //    $sql = "select * from " . $ecs->table( 'users') . ' where user_id = ' . $user_id;
    //    $referral_code = $db->getAll( $sql)[0]['referral_code'];
    $referral_code=1;
    //引入phpqrcode库文件
    ob_start();
    include('phpqrcode/phpqrcode.php');
    $url =  "http://" . $_SERVER['HTTP_HOST'] . "/user.php?act=register&referral_code=" . $referral_code;
    // 生成的文件名
    $name = time();
    $filename = $name.'.png';
    // 纠错级别：L、M、Q、H
    $errorCorrectionLevel = 'L';
    // 点的大小：1到10
    $matrixPointSize = 10;
    //输入二维码到浏览器
    QRcode::png($url);
    $imageString = base64_encode(ob_get_contents());
    //关闭缓冲区
    ob_end_clean();
    //把生成的base64字符串返回给前端
    $data = array(
        'code'=>200,
        'data'=>['qrcode' => $imageString, 'url' => $url, 'referral_code' => $referral_code]
    );
    exit( json_encode( $data));